import { World } from '@feng3d-plugins/cannon';
import { Vector3 } from 'feng3d';

export { };

World.worldNormal = new Vector3(0, 1, 0) as any;
