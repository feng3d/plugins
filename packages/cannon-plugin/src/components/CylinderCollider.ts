import { Cylinder } from '@feng3d-plugins/cannon';
import { AddComponentMenu, oav, RegisterComponent, Serializable, SerializeProperty } from 'feng3d';
import { Collider } from './Collider';

declare global
{
    export interface MixinsComponentMap
    {
        CylinderCollider: CylinderCollider;
    }
}

export interface CylinderCollider
{
    get shape(): Cylinder;
}

/**
 * 圆柱体碰撞体
 */
@AddComponentMenu('Physics/Cylinder Collider')
@RegisterComponent({ name: 'CylinderCollider' })
@Serializable('CylinderCollider')
export class CylinderCollider extends Collider
{
    /**
     * 顶部半径
     */
    @oav()
    @SerializeProperty()
    topRadius = 0.5;

    /**
     * 底部半径
     */
    @oav()
    @SerializeProperty()
    bottomRadius = 0.5;

    /**
     * 高度
     */
    @oav()
    @SerializeProperty()
    height = 2;

    /**
     * 横向分割数
     */
    @oav()
    @SerializeProperty()
    segmentsW = 16;

    init()
    {
        this._shape = new Cylinder(this.topRadius, this.bottomRadius, this.height, this.segmentsW);
    }
}
