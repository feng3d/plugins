import { Body } from '@feng3d-plugins/cannon';
import { AddComponentMenu, oav, RegisterComponent, RunEnvironment, Scene, Serializable, SerializeProperty, Vector3 } from 'feng3d';
import { Component3D } from 'feng3d/src/core/core/Component3D';
import { Collider } from './Collider';

declare global
{
    export interface MixinsComponentMap
    {
        Rigidbody: Rigidbody;
    }
}

/**
 * 刚体
 */
@AddComponentMenu('Physics/Rigidbody')
@RegisterComponent({ name: 'Rigidbody' })
@Serializable('Rigidbody')
export class Rigidbody extends Component3D
{
    declare __class__: 'physics.Rigidbody';

    body = new Body();

    runEnvironment = RunEnvironment.feng3d;

    @oav()
    @SerializeProperty()
    get mass()
    {
        return this.body.mass;
    }
    set mass(v)
    {
        this.body.mass = v;
    }

    init()
    {
        this.body = new Body({ mass: this.mass });

        this.body.position = new Vector3(this.node3d.position.x, this.node3d.position.y, this.node3d.position.z) as any;

        const colliders = this.node3d.getComponents(Collider);
        colliders.forEach((element) =>
        {
            this.body.addShape(element.shape);
        });

        this.on('matrixChanged', this._onTransformChanged, this);
    }

    private _onTransformChanged()
    {
        this.body.position = new Vector3(this.node3d.position.x, this.node3d.position.y, this.node3d.position.z) as any;
    }

    /**
     * 每帧执行
     */
    update(_interval?: number)
    {
        const scene = this.getComponentsInParent(Scene)[0];
        if (scene)
        {
            this.node3d.position = new Vector3(this.body.position.x, this.body.position.y, this.body.position.z);
        }
    }
}
