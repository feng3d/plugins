import path from 'path';
import dts from 'rollup-plugin-dts';
import json from 'rollup-plugin-json';
import typescript from 'rollup-plugin-typescript';
import pkg from './package.json';

/**
 * Get a list of the non-private sorted packages with Lerna v3
 * @see https://github.com/lerna/lerna/issues/1848
 * @return {Promise<Package[]>} List of packages
 */
async function main()
{
    const results = [];

    const {
        standalone,
    } = pkg;

    results.push({
        input: path.relative(__dirname, 'src/index.ts'),
        external: standalone ? [] : Object.keys(pkg.dependencies || []),
        output: [{
            file: path.relative(__dirname, pkg.dts),
            name: `${pkg.namespace}`,
            format: 'es',
            footer: `export as namespace ${pkg.namespace};`
        }],
        plugins: [
            json(),
            typescript({ tsconfig: './tsconfig.json' }),
            dts({ respectExternal: true }),
        ],
    });

    return results;
}

export default main();
