import { Vector3, Quaternion } from 'feng3d';
import { Heightfield } from '../../src/shapes/Heightfield';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('Heightfield', () =>
{
    it('calculateWorldAABB', () =>
    {
        const hfShape = createHeightfield({
            elementSize: 1,
            minValue: 0
        });
        const min = new Vector3();
        const max = new Vector3();
        hfShape.calculateWorldAABB(
            new Vector3(),
            new Quaternion(),
            min,
            max
        );

        equal(min.x, -Number.MAX_VALUE);
        equal(max.x, Number.MAX_VALUE);
        equal(min.y, -Number.MAX_VALUE);
        equal(max.y, Number.MAX_VALUE);
    });

    it('getConvexTrianglePillar', () =>
    {
        const hfShape = createHeightfield({
            elementSize: 1,
            minValue: 0,
            size: 2
        });

        hfShape.getConvexTrianglePillar(0, 0, false);
        equal(hfShape.pillarConvex.vertices.length, 6);
        deepEqual(hfShape.pillarConvex.vertices.slice(0, 3), [
            new Vector3(-0.25, -0.25, 0.5),
            new Vector3(0.75, -0.25, 0.5),
            new Vector3(-0.25, 0.75, 0.5)
        ]);
        deepEqual(hfShape.pillarOffset, new Vector3(0.25, 0.25, 0.5));

        hfShape.getConvexTrianglePillar(0, 0, true);
        equal(hfShape.pillarConvex.vertices.length, 6);
        deepEqual(hfShape.pillarConvex.vertices.slice(0, 3), [
            new Vector3(0.25, 0.25, 0.5),
            new Vector3(-0.75, 0.25, 0.5),
            new Vector3(0.25, -0.75, 0.5)
        ]);
        deepEqual(hfShape.pillarOffset, new Vector3(0.75, 0.75, 0.5));

        // Out of bounds
        throws(function ()
        {
            hfShape.getConvexTrianglePillar(1, 1, true);
        }, Error);
        throws(function ()
        {
            hfShape.getConvexTrianglePillar(1, 1, false);
        }, Error);
        throws(function ()
        {
            hfShape.getConvexTrianglePillar(-1, 0, false);
        }, Error);
    });

    it('getTriangle', () =>
    {
        const hfShape = createHeightfield({
            elementSize: 1,
            minValue: 0,
            size: 2
        });
        const a = new Vector3();
        const b = new Vector3();
        const c = new Vector3();

        hfShape.getTriangle(0, 0, false, a, b, c);
        deepEqual(a, new Vector3(0, 0, 1));
        deepEqual(b, new Vector3(1, 0, 1));
        deepEqual(c, new Vector3(0, 1, 1));

        hfShape.getTriangle(0, 0, true, a, b, c);
        deepEqual(a, new Vector3(1, 1, 1));
        deepEqual(b, new Vector3(0, 1, 1));
        deepEqual(c, new Vector3(1, 0, 1));
    });

    it('getRectMinMax', () =>
    {
        const hfShape = createHeightfield();
        const minMax = [];
        hfShape.getRectMinMax(0, 0, 1, 1, minMax);
        deepEqual(minMax, [1, 1]);
    });

    it('getHeightAt', () =>
    {
        const hfShape = createHeightfield({
            size: 2,
            elementSize: 1,
            linear: true
        });
        const h0 = hfShape.getHeightAt(0, 0);
        const h1 = hfShape.getHeightAt(0.25, 0.25);
        const h2 = hfShape.getHeightAt(0.75, 0.75);
        const h3 = hfShape.getHeightAt(0.99, 0.99);

        equal(h0, 0);
        ok(h0 < h1);
        ok(h1 < h2);
        ok(h2 < h3);
    });

    it('update', () =>
    {
        const hfShape = createHeightfield();
        hfShape.update();

        ok(true);
    });

    it('updateMaxValue', () =>
    {
        const hfShape = createHeightfield();
        hfShape.data[0][0] = 10;
        hfShape.updateMaxValue();
        equal(hfShape.maxValue, 10);
    });

    it('updateMinValue', () =>
    {
        const hfShape = createHeightfield();
        hfShape.data[0][0] = -10;
        hfShape.updateMinValue();
        equal(hfShape.minValue, -10);
    });

    it('setHeightValueAtIndex', () =>
    {
        const hfShape = createHeightfield();
        hfShape.setHeightValueAtIndex(0, 0, 10);
        equal(hfShape.data[0][0], 10);
    });

    it('getIndexOfPosition', () =>
    {
        const hfShape = createHeightfield();
        const result = [];
        hfShape.getIndexOfPosition(0, 0, result);
        deepEqual(result, [0, 0]);
    });
});

function createHeightfield(options?)
{
    options = options || {};
    const matrix: number[][] = [];
    const size = options.size || 20;
    for (let i = 0; i < size; i++)
    {
        matrix.push([]);
        for (let j = 0; j < size; j++)
        {
            if (options.linear)
            {
                matrix[i].push(i + j);
            }
            else
            {
                matrix[i].push(1);
            }
        }
    }
    const hfShape = new Heightfield(matrix, options);

    return hfShape;
}
