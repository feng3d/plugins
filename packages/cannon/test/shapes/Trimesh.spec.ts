import { Quaternion, Vector3 } from 'feng3d';

import { Body } from '../../src/objects/Body';
import { Plane } from '../../src/shapes/Plane';
import { Trimesh } from '../../src/shapes/Trimesh';
import { World } from '../../src/world/World';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('Trimesh', () =>
{
    it('updateNormals', () =>
    {
        const mesh = Trimesh.createTorus();
        mesh.normals[0] = 1;
        mesh.updateNormals();
        ok(mesh.normals[0] !== 1);
    });

    it('updateAABB', () =>
    {
        const mesh = Trimesh.createTorus();
        mesh.aabb.min.set(1, 2, 3);
        mesh.updateAABB();
        ok(mesh.aabb.min.y !== 2);
    });

    it('updateTree scaled', () =>
    {
        const mesh = Trimesh.createTorus();
        mesh.updateTree();

        const bigMesh = Trimesh.createTorus();
        bigMesh.setScale(new Vector3(2, 2, 2));

        equal(bigMesh.aabb.max.x, mesh.aabb.max.x * 2, 'AABB does not scale with the mesh!');

        equal(bigMesh.tree.aabb.max.x, mesh.tree.aabb.max.x, 'Octree AABB scales with the mesh, which is wrong!');
    });

    it('getTrianglesInAABB unscaled', () =>
    {
        const mesh = Trimesh.createTorus(1, 1, 32, 32);
        const result: number[] = [];

        // Should get all triangles if we use the full AABB
        const aabb = mesh.aabb.clone();
        mesh.getTrianglesInAABB(aabb, result);
        equal(result.length, mesh.indices.length / 3);

        // Should get less triangles if we use the half AABB
        result.length = 0;
        aabb.min.scaleNumberTo(0.1, aabb.max);
        aabb.max.scaleNumberTo(0.1, aabb.max);
        mesh.getTrianglesInAABB(aabb, result);

        console.log(result.length, mesh.indices.length / 3);

        ok(result.length < mesh.indices.length / 3);
    });

    // scaled: function(test){
    //     var mesh = Trimesh.createTorus(1,1,16,16);
    //     var result = [];

    //     // Should get all triangles if we use the full AABB
    //     var aabb = mesh.aabb.clone();
    //     mesh.getTrianglesInAABB(aabb, result);
    //     equal(result.length, mesh.indices.length / 3);

    //     // Should get less triangles if we use the half AABB
    //     result.length = 0;
    //     aabb.min.scaleNumberTo(0.5, aabb.lowerBound);
    //     aabb.max.scaleNumberTo(0.5, aabb.upperBound);
    //     mesh.getTrianglesInAABB(aabb, result);
    //     ok(result.length < mesh.indices.length / 3);

    //     test.done();
    // }

    it('getVertex unscaled', () =>
    {
        const mesh = Trimesh.createTorus();
        const vertex = new Vector3();
        mesh.getVertex(0, vertex);
        deepEqual(vertex, new Vector3(mesh.vertices[0], mesh.vertices[1], mesh.vertices[2]));
    });

    it('getVertex scaled', () =>
    {
        const mesh = Trimesh.createTorus();
        mesh.setScale(new Vector3(1, 2, 3));
        const vertex = new Vector3();
        mesh.getVertex(0, vertex);
        deepEqual(vertex, new Vector3(Number(mesh.vertices[0]), 2 * mesh.vertices[1], 3 * mesh.vertices[2]));
    });

    it('getWorldVertex', () =>
    {
        const mesh = Trimesh.createTorus();
        const vertex = new Vector3();
        mesh.getWorldVertex(0, new Vector3(), new Quaternion(), vertex);
        deepEqual(vertex, new Vector3(mesh.vertices[0], mesh.vertices[1], mesh.vertices[2]));
    });

    it('getTriangleVertices', () =>
    {
        const mesh = Trimesh.createTorus();
        const va = new Vector3();
        const vb = new Vector3();
        const vc = new Vector3();
        const va1 = new Vector3();
        const vb1 = new Vector3();
        const vc1 = new Vector3();
        mesh.getVertex(mesh.indices[0], va);
        mesh.getVertex(mesh.indices[1], vb);
        mesh.getVertex(mesh.indices[2], vc);
        mesh.getTriangleVertices(0, va1, vb1, vc1);
        deepEqual(va, va1);
        deepEqual(vb, vb1);
        deepEqual(vc, vc1);
    });

    it('getNormal', () =>
    {
        const mesh = Trimesh.createTorus();
        const normal = new Vector3();
        mesh.getNormal(0, normal);
        deepEqual(new Vector3(mesh.normals[0], mesh.normals[1], mesh.normals[2]), normal);
    });

    it('calculateLocalInertia', () =>
    {
        const mesh = Trimesh.createTorus();
        const inertia = new Vector3();
        mesh.calculateLocalInertia(1, inertia);
        ok(true);
    });

    it('computeLocalAABB', () =>
    {
        console.log('Trimesh::computeLocalAABB is todo');
        ok(true);
    });

    it('updateBoundingSphereRadius', () =>
    {
        console.log('Trimesh::updateBoundingSphereRadius is todo');
        ok(true);
    });

    it('calculateWorldAABB', () =>
    {
        const poly = Trimesh.createTorus();
        const min = new Vector3();
        const max = new Vector3();
        poly.calculateWorldAABB(
            new Vector3(1, 0, 0), // Translate 2 x in world
            new Quaternion(0, 0, 0, 1),
            min,
            max
        );
        ok(!isNaN(min.x));
        ok(!isNaN(max.x));
    });

    it('volume', () =>
    {
        const mesh = Trimesh.createTorus();
        ok(mesh.volume() > 0);
    });

    it('narrowphaseAgainstPlane', () =>
    {
        const world = new World();

        const torusShape = Trimesh.createTorus();
        const torusBody = new Body({
            mass: 1
        });
        torusBody.addShape(torusShape);

        const planeBody = new Body({
            mass: 1
        });
        planeBody.addShape(new Plane());

        world.addBody(torusBody);
        world.addBody(planeBody);

        world.step(1 / 60);
        ok(true);
    });
});
