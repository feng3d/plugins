import { Quaternion, Vector3 } from 'feng3d';

import { Box } from '../../src/shapes/Box';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual } = assert;

describe('Box', () =>
{
    it('forEachWOrldCorner', () =>
    {
        const box = new Box(new Vector3(1, 1, 1));
        const pos = new Vector3();
        const quat = new Quaternion();
        quat.fromAxisAngle(new Vector3(0, 0, 1), Math.PI * 0.25);
        let numCorners = 0;
        const unique: Vector3[] = [];
        box.forEachWorldCorner(pos, quat, function (x, y, z)
        {
            const corner = new Vector3(x, y, z);
            for (let i = 0; i < unique.length; i++)
            {
                ok(!corner.equals(unique[i]), `Corners ${i} and ${numCorners} are almost equal: (${unique[i].toString()}) == (${corner.toString()})`);
            }
            unique.push(corner);
            numCorners++;
        });
        equal(numCorners, 8);
    });

    it('calculateWorldAABB', () =>
    {
        const box = new Box(new Vector3(1, 1, 1));
        const min = new Vector3();
        const max = new Vector3();
        box.calculateWorldAABB(new Vector3(3, 0, 0),
            new Quaternion(0, 0, 0, 1),
            min,
            max);
        equal(min.x, 2);
        equal(max.x, 4);
        equal(min.y, -1);
        equal(max.y, 1);
    });
});
