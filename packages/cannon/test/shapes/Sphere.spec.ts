import { Sphere } from '../../src/shapes/Sphere';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('Sphere', () =>
{
    it('throwOnWrongRadius', () =>
    {
        // These should be all right
        new Sphere(1);
        new Sphere(0);

        throws(function ()
        {
            new Sphere(-1);
        }, Error, 'The sphere radius cannot be negative.');

        ok(true);
    });
});
