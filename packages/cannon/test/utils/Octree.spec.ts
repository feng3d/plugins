import { Box3, Vector3 } from 'feng3d';
import { Octree } from '../../src/utils/Octree';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('Octree', () =>
{
    it('construct', () =>
    {
        const tree = new Octree(new Box3());
        ok(true);
    });

    it('insertRoot', () =>
    {
        const aabb = new Box3(new Vector3(-1, -1, -1), new Vector3(1, 1, 1));
        const tree = new Octree(aabb);

        const nodeAABB = new Box3(new Vector3(-1, -1, -1), new Vector3(1, 1, 1));
        const nodeData = 123;
        tree.insert(nodeAABB, nodeData);

        // Should end up in root node and not children
        equal(tree.data.length, 1);
        equal(tree.children.length, 0);
    });

    it('insertDeep', () =>
    {
        const aabb = new Box3(new Vector3(-1, -1, -1), new Vector3(1, 1, 1));
        const tree = new Octree(aabb, {
            maxDepth: 8
        });

        const nodeAABB = new Box3(new Vector3(-1, -1, -1), new Vector3(-1, -1, -1));
        const nodeData = 123;

        tree.insert(nodeAABB, nodeData);

        // Should be deep (maxDepth deep) in lower corner
        ok(
            tree // level 0
                .children[0] // 1
                .children[0] // 2
                .children[0] // 3
                .children[0] // 4
                .children[0] // 5
                .children[0] // 6
                .children[0] // 7
                .children[0] // 8
        );
        equal(tree.data.length, 0);
    });

    it('aabbQuery', () =>
    {
        const aabb = new Box3(new Vector3(-1, -1, -1), new Vector3(1, 1, 1));
        const tree = new Octree<number>(aabb);

        const nodeAABB = new Box3(new Vector3(-1, -1, -1), new Vector3(1, 1, 1));
        const nodeData = 123;

        tree.insert(nodeAABB, nodeData);

        let result: number[] = [];
        tree.aabbQuery(aabb, result);

        deepEqual(result, [123]);

        const nodeAABB2 = new Box3(new Vector3(-1, -1, -1), new Vector3(-1, -1, -1));
        const nodeData2 = 456;
        tree.insert(nodeAABB2, nodeData2);

        result = [];
        tree.aabbQuery(aabb, result);
        deepEqual(result, [123, 456]);

        result = [];
        tree.aabbQuery(new Box3(new Vector3(0, 0, 0), new Vector3(1, 1, 1)), result);
        deepEqual(result, [123]);
    });
});
