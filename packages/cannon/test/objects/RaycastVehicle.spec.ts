import { Vector3 } from 'feng3d';
import { Body } from '../../src/objects/Body';
import { RaycastVehicle } from '../../src/objects/RaycastVehicle';
import { Plane } from '../../src/shapes/Plane';
import { World } from '../../src/world/World';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('RaycastVehicle', () =>
{
    it('construct', () =>
    {
        const vehicle = new RaycastVehicle({
            chassisBody: new Body()
        });
        ok(true);
    });

    it('addWheel', () =>
    {
        const vehicle = new RaycastVehicle({
            chassisBody: new Body()
        });
        vehicle.addWheel({});
        equal(vehicle.wheelInfos.length, 1);
    });

    it('addWheel1', () =>
    {
        const vehicle = new RaycastVehicle({
            chassisBody: new Body()
        });
        vehicle.addWheel({});
        equal(vehicle.wheelInfos.length, 1);
        vehicle.addWheel({});
        equal(vehicle.wheelInfos.length, 2);
    });

    it('setSteeringValue', () =>
    {
        const vehicle = createVehicle();
        vehicle.setSteeringValue(Math.PI / 4, 0);
        ok(true);
    });

    it('applyEngineForce', () =>
    {
        const vehicle = createVehicle();
        vehicle.applyEngineForce(1000, 0);
        ok(true);
    });

    it('setBrake', () =>
    {
        const vehicle = createVehicle();
        vehicle.applyEngineForce(1000, 0);
        ok(true);
    });

    it('updateSuspension', () =>
    {
        const vehicle = createVehicle();
        vehicle.updateSuspension(1 / 60);
        ok(true);
    });

    it('updateFriction', () =>
    {
        const vehicle = createVehicle();
        vehicle.updateFriction(1 / 60);
        ok(true);
    });

    it('updateWheelTransform', () =>
    {
        const vehicle = createVehicle();
        vehicle.updateWheelTransform(0);
        ok(true);
    });

    it('updateVehicle', () =>
    {
        const vehicle = createVehicle();
        vehicle.updateVehicle(1 / 60);
        ok(true);
    });

    it('getVehicleAxisWorld', () =>
    {
        const vehicle = createVehicle();
        const v = new Vector3();

        vehicle.getVehicleAxisWorld(0, v);
        deepEqual(v, new Vector3(1, 0, 0));

        vehicle.getVehicleAxisWorld(1, v);
        deepEqual(v, new Vector3(0, 1, 0));

        vehicle.getVehicleAxisWorld(2, v);
        deepEqual(v, new Vector3(0, 0, 1));

        ok(true);
    });

    it('removeFromWorld', () =>
    {
        const world = new World();
        const vehicle = new RaycastVehicle({
            chassisBody: new Body({ mass: 1 })
        });

        vehicle.addToWorld(world);
        ok(world.bodies.indexOf(vehicle.chassisBody) !== -1);
        ok(world.has('preStep'));

        vehicle.removeFromWorld(world);
        ok(world.bodies.indexOf(vehicle.chassisBody) === -1);
        ok(!world.has('preStep'));
    });
});

function createVehicle()
{
    const vehicle = new RaycastVehicle({
        chassisBody: new Body({
            mass: 1
        })
    });
    const down = new Vector3(0, 0, -1);
    const info = {
        chassisConnectionPointLocal: new Vector3(-5, -1 / 2, 0),
        axleLocal: new Vector3(0, -1, 0),
        directionLocal: down,
        suspensionStiffness: 1000,
        suspensionRestLength: 2,
    };
    vehicle.addWheel(info);

    const world = new World();
    const planeBody = new Body();
    planeBody.position.z = -1;
    planeBody.addShape(new Plane());
    world.addBody(planeBody);

    vehicle.addToWorld(world);

    return vehicle;
}
