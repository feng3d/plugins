import { Quaternion, Vector3 } from 'feng3d';
import { Body } from '../../src/objects/Body';
import { Box } from '../../src/shapes/Box';
import { Sphere } from '../../src/shapes/Sphere';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual } = assert;

describe('Body', () =>
{
    it('computeAABB box', () =>
    {
        const body = new Body({ mass: 1 });
        body.addShape(new Box(new Vector3(1, 1, 1)));
        body.computeAABB();
        equal(body.aabb.min.x, -1);
        equal(body.aabb.min.y, -1);
        equal(body.aabb.min.z, -1);
        equal(body.aabb.max.x, 1);
        equal(body.aabb.max.y, 1);
        equal(body.aabb.max.z, 1);

        body.position.x = 1;
        body.computeAABB();

        equal(body.aabb.min.x, 0);
        equal(body.aabb.max.x, 2);
    });

    it('computeAABB boxOffset', () =>
    {
        const quaternion = new Quaternion();
        quaternion.fromAxisAngle(new Vector3(0, 0, 1), Math.PI / 2);
        const body = new Body({ mass: 1 });
        body.addShape(new Box(new Vector3(1, 1, 1)), new Vector3(1, 1, 1));
        body.computeAABB();
        equal(body.aabb.min.x, 0);
        equal(body.aabb.min.y, 0);
        equal(body.aabb.min.z, 0);
        equal(body.aabb.max.x, 2);
        equal(body.aabb.max.y, 2);
        equal(body.aabb.max.z, 2);

        body.position.x = 1;
        body.computeAABB();

        equal(body.aabb.min.x, 1);
        equal(body.aabb.max.x, 3);
    });

    it('updateInertiaWorld', () =>
    {
        const body = new Body({ mass: 1 });
        body.addShape(new Box(new Vector3(1, 1, 1)));
        body.quaternion.fromEuler(Math.PI / 2, 0, 0);
        body.updateInertiaWorld();
        ok(true);
    });

    it('pointToLocalFrame', () =>
    {
        const body = new Body({ mass: 1 });
        body.addShape(new Sphere(1));
        body.position.set(1, 2, 2);
        const localPoint = body.pointToLocalFrame(new Vector3(1, 2, 3));
        ok(localPoint.equals(new Vector3(0, 0, 1)));
    });

    it('pointToWorldFrame', () =>
    {
        const body = new Body({ mass: 1 });
        body.addShape(new Sphere(1));
        body.position.set(1, 2, 2);
        const worldPoint = body.pointToWorldFrame(new Vector3(1, 0, 0));
        ok(worldPoint.equals(new Vector3(2, 2, 2)));
    });

    it('addShape', () =>
    {
        const sphereShape = new Sphere(1);

        const bodyA = new Body({
            mass: 1,
            shape: sphereShape
        });
        const bodyB = new Body({
            mass: 1
        });
        bodyB.addShape(sphereShape);

        deepEqual(bodyA.shapes, bodyB.shapes, 'Adding shape via options did not work.');
        deepEqual(bodyA.inertia, bodyB.inertia);
    });

    it('applyForce', () =>
    {
        const sphereShape = new Sphere(1);
        const body = new Body({
            mass: 1,
            shape: sphereShape
        });

        const worldPoint = new Vector3(1, 0, 0);
        const forceVector = new Vector3(0, 1, 0);
        body.applyForce(forceVector, worldPoint);
        deepEqual(body.force, forceVector);
        deepEqual(body.torque, new Vector3(0, 0, 1));
    });

    it('applyLocalForce', () =>
    {
        const sphereShape = new Sphere(1);
        const body = new Body({
            mass: 1,
            shape: sphereShape
        });
        body.quaternion.fromAxisAngle(new Vector3(1, 0, 0), Math.PI / 2);

        const localPoint = new Vector3(1, 0, 0);
        const localForceVector = new Vector3(0, 1, 0);
        body.applyLocalForce(localForceVector, localPoint);
        ok(body.force.equals(new Vector3(0, 0, 1))); // The force is rotated to world space
    });

    it('applyImpulse', () =>
    {
        const sphereShape = new Sphere(1);
        const body = new Body({
            mass: 1,
            shape: sphereShape
        });

        const f = 1000;
        const dt = 1 / 60;
        const worldPoint = new Vector3(0, 0, 0);
        const impulse = new Vector3(f * dt, 0, 0);
        body.applyImpulse(impulse, worldPoint);

        ok(body.velocity.equals(new Vector3(f * dt, 0, 0)));
    });

    it('applyLocalImpulse', () =>
    {
        const sphereShape = new Sphere(1);
        const body = new Body({
            mass: 1,
            shape: sphereShape
        });
        body.quaternion.fromAxisAngle(new Vector3(1, 0, 0), Math.PI / 2);

        const f = 1000;
        const dt = 1 / 60;
        const localPoint = new Vector3(1, 0, 0);
        const localImpulseVector = new Vector3(0, f * dt, 0);
        body.applyLocalImpulse(localImpulseVector, localPoint);
        ok(body.velocity.equals(new Vector3(0, 0, f * dt))); // The force is rotated to world space
    });
});
