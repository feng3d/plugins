import { assert, describe, it } from 'vitest';
import { OverlapKeeper } from '../../src/collision/OverlapKeeper';
const { ok, equal, deepEqual, throws } = assert;

describe('OverlapKeeper', () =>
{
	it('construct', () =>
	{
		new OverlapKeeper();
		ok(true);
	});

	it('set', () =>
	{
		const keeper = new OverlapKeeper();

		keeper.set(1, 2);
		deepEqual(keeper.current, [keeper.getKey(1, 2)]);

		keeper.set(3, 2);
		deepEqual(keeper.current, [keeper.getKey(1, 2), keeper.getKey(3, 2)]);

		keeper.set(3, 1);
		deepEqual(keeper.current, [keeper.getKey(1, 2), keeper.getKey(1, 3), keeper.getKey(3, 2)]);
	});

	it('getDiff', () =>
	{
		const keeper = new OverlapKeeper();

		keeper.set(1, 2);
		keeper.set(3, 2);
		keeper.set(3, 1);

		keeper.tick();

		keeper.set(1, 2);
		keeper.set(3, 2);
		keeper.set(3, 1);

		let additions = [];
		let removals = [];
		keeper.getDiff(additions, removals);

		equal(additions.length, 0);
		equal(removals.length, 0);

		keeper.tick();

		keeper.set(1, 2);
		keeper.getDiff(additions, removals);
		equal(additions.length, 0);
		deepEqual(removals, [1, 3, 2, 3]);

		keeper.tick();

		keeper.set(1, 2);
		keeper.set(1, 2);

		additions = [];
		removals = [];
		keeper.getDiff(additions, removals);
		equal(additions.length, 0, 'should handle duplicate entries');
		equal(removals.length, 0, 'should handle duplicate entries');

		keeper.set(3, 2);
		keeper.set(3, 1);
		additions = [];
		removals = [];
		keeper.getDiff(additions, removals);
		deepEqual(additions, [1, 3, 2, 3]);

		keeper.tick();

		keeper.set(4, 2);
		keeper.set(4, 1);

		additions = [];
		removals = [];
		keeper.getDiff(additions, removals);
		deepEqual(additions, [1, 4, 2, 4]);
		deepEqual(removals, [1, 2, 1, 3, 2, 3]);
	});
});
