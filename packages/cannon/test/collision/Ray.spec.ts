import { Quaternion, Vector3 } from 'feng3d';
import { Ray } from '../../src/collision/Ray';
import { RaycastResult } from '../../src/collision/RaycastResult';
import { Body } from '../../src/objects/Body';
import { Box } from '../../src/shapes/Box';
import { Heightfield } from '../../src/shapes/Heightfield';
import { Trimesh } from '../../src/shapes/Trimesh';

import { assert, describe, it } from 'vitest';
import { Sphere } from '../../src/shapes/Sphere';
import { Plane } from '../../src/shapes/Plane';
const { ok, equal, deepEqual, throws } = assert;

describe('Ray', () =>
{
    it('construct', () =>
    {
        const r = new Ray(new Vector3(), new Vector3(1, 0, 0));
        ok(true);
    });

    it('intersectBody', () =>
    {
        let r = new Ray(new Vector3(5, 0, 0), new Vector3(-5, 0, 0));
        r.skipBackfaces = true;
        const shape = createPolyhedron(0.5);
        const body = new Body({ mass: 1 });
        body.addShape(shape);

        const result = new RaycastResult();

        r.intersectBody(body, result);
        ok(result.hasHit);
        ok(result.hitPointWorld.equals(new Vector3(0.5, 0, 0)));

        // test rotating the body first
        result.reset();
        body.quaternion.fromAxisAngle(new Vector3(1, 0, 0), Math.PI);
        r.intersectBody(body, result);
        ok(result.hasHit);
        ok(result.hitPointWorld.equals(new Vector3(0.5, 0, 0)));

        // test shooting from other direction
        result.reset();
        r.to.set(0, 0, -5);
        r.from.set(0, 0, 5);
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0, 0, 0.5)));

        // test miss
        result.reset();
        r = new Ray(new Vector3(5, 1, 0), new Vector3(-5, 1, 0));
        r.intersectBody(body, result);
        equal(result.hasHit, false);

        ok(true);
    });

    it('intersectBodies', () =>
    {
        const r = new Ray(new Vector3(5, 0, 0), new Vector3(-5, 0, 0));
        r.skipBackfaces = true;
        const shape = createPolyhedron(0.5);
        const body1 = new Body({ mass: 1 });
        body1.addShape(shape);
        const body2 = new Body({ mass: 1 });
        body2.addShape(shape);
        body2.position.x = -2;

        const result = new RaycastResult();
        r.intersectBodies([body1, body2], result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0.5, 0, 0)));
    });

    it('box', () =>
    {
        const r = new Ray(new Vector3(5, 0, 0), new Vector3(-5, 0, 0));
        r.skipBackfaces = true;
        const shape = new Box(new Vector3(0.5, 0.5, 0.5));
        const body = new Body({ mass: 1 });
        body.addShape(shape);
        const result = new RaycastResult();

        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0.5, 0, 0)));

        result.reset();
        body.quaternion.fromAxisAngle(new Vector3(1, 0, 0), Math.PI / 2);
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0.5, 0, 0)));

        result.reset();
        body.quaternion.fromAxisAngle(new Vector3(1, 0, 0), Math.PI);
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0.5, 0, 0)));

        result.reset();
        body.quaternion.fromAxisAngle(new Vector3(1, 0, 0), 3 * Math.PI / 2);
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0.5, 0, 0)));
    });

    it('sphere', () =>
    {
        const r = new Ray(new Vector3(5, 0, 0), new Vector3(-5, 0, 0));
        r.skipBackfaces = true;
        const shape = new Sphere(1);
        const body = new Body({ mass: 1 });
        body.addShape(shape);

        const result = new RaycastResult();
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(1, 0, 0)));

        result.reset();
        body.position.set(1, 0, 0);
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(2, 0, 0)));

        result.reset();
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(2, 0, 0)));

        result.reset();
        const shape2 = new Sphere(1);
        const body2 = new Body({ mass: 1 });
        body2.addShape(shape2, new Vector3(1, 0, 0));
        r.intersectBody(body2, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(2, 0, 0)));
    });

    it('heightfield', () =>
    {
        const r = new Ray(new Vector3(0, 0, 10), new Vector3(0, 0, -10));
        r.skipBackfaces = true;
        const data = [
            [1, 1, 1],
            [1, 1, 1],
            [1, 1, 1]
        ];
        const shape = new Heightfield(data, {
            elementSize: 1
        });
        const body = new Body({ mass: 1 });
        body.addShape(shape);

        // Hit
        {
            const result = new RaycastResult();
            r.intersectBody(body, result);
            equal(result.hasHit, true);
            deepEqual(result.hitPointWorld, new Vector3(0, 0, 1));
        }

        // Miss
        {
            const result = new RaycastResult();
            r.from.set(-100, -100, 10);
            r.to.set(-100, -100, -10);
            r.intersectBody(body, result);
            equal(result.hasHit, false);
        }
        {
            // Hit all triangles!
            const result = new RaycastResult();
            for (let i = 0; i < data.length - 1; i++)
            { // 3x3 data points will have 2x2 rectangles in the field
                for (let j = 0; j < data[i].length - 1; j++)
                {
                    for (let k = 0; k < 2; k++)
                    {
                        result.reset();
                        r.from.set(i + 0.25, j + 0.25, 10);
                        r.to.set(i + 0.25, j + 0.25, -10);
                        if (k)
                        {
                            r.from.x += 0.5;
                            r.from.y += 0.5;
                            r.to.x += 0.5;
                            r.to.y += 0.5;
                        }
                        r.intersectBody(body, result);
                        ok(result.hasHit, `missed triangle ${[i, j].join(',')}`);
                    }
                }
            }
        }

        ok(true);
    });

    it('plane', () =>
    {
        let r = new Ray(new Vector3(0, 0, 5), new Vector3(0, 0, -5));
        r.skipBackfaces = true;
        const shape = new Plane();
        const body = new Body({ mass: 1 });
        body.addShape(shape);

        let result = new RaycastResult();
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0, 0, 0)));
        equal(result.distance, 5);

        result.reset();
        const body2 = new Body({ mass: 1 });
        body2.addShape(shape, new Vector3(0, 0, 1), new Quaternion());
        r.intersectBody(body2, result);
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0, 0, 1)));

        result.reset();
        const body3 = new Body({ mass: 1 });
        const quat = new Quaternion();
        quat.fromAxisAngle(new Vector3(1, 0, 0), Math.PI / 2);
        body3.addShape(shape, new Vector3(), quat);
        r.intersectBody(body3, result);
        equal(result.hasHit, false);

        result.reset();
        const body4 = new Body({ mass: 1 });
        body4.addShape(shape);
        r = new Ray(new Vector3(1, 1, 5), new Vector3(1, 1, -5));
        r.intersectBody(body4, result);
        equal(result.hasHit, true);
        deepEqual(result.hitPointWorld, new Vector3(1, 1, 0));
        equal(result.distance, 5);

        result = new RaycastResult();
        r.from.set(0, 1, 1);
        r.to.set(0, -1, -1);
        body.position.set(0, 0, 0);
        r.intersectBody(body, result);
        const distance1 = result.distance;
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0, 0, 0)));

        result = new RaycastResult();
        r.from.set(0, 1 - 5, 1);
        r.to.set(0, -1 - 5, -1);
        body.position.set(0, 0, 0);
        r.intersectBody(body, result);
        const distance2 = result.distance;
        equal(result.hasHit, true);
        ok(result.hitPointWorld.equals(new Vector3(0, -5, 0)));
        equal(distance1, distance2);

        ok(true);
    });

    it('trimesh', () =>
    {
        const r = new Ray(new Vector3(0.5, 0.5, 10), new Vector3(0.5, 0.5, -10));
        r.skipBackfaces = true;

        const vertices = [
            0, 0, 0,
            1, 0, 0,
            0, 1, 0
        ];
        const indices = [
            0, 1, 2
        ];

        const body = new Body({
            mass: 1,
            shape: new Trimesh(vertices, indices)
        });

        // Hit
        let result = new RaycastResult();
        r.intersectBody(body, result);
        equal(result.hasHit, true);
        deepEqual(result.hitPointWorld, new Vector3(0.5, 0.5, 0));

        // Miss
        result = new RaycastResult();
        r.from.set(-100, -100, 10);
        r.to.set(-100, -100, -10);
        r.intersectBody(body, result);
        equal(result.hasHit, false);
    });
});

function createPolyhedron(size)
{
    size = (size === undefined ? 0.5 : size);
    const box = new Box(new Vector3(size, size, size));
    box.updateConvexPolyhedronRepresentation();

    return box.convexPolyhedronRepresentation;
}
