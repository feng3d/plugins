import { Vector3 } from 'feng3d';
import { HingeConstraint } from '../../src/constraints/HingeConstraint';
import { Body } from '../../src/objects/Body';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('HingeConstraint', () =>
{
    it('construct', () =>
    {
        const bodyA = new Body({ mass: 1, position: new Vector3(1, 0, 0) });
        const bodyB = new Body({ mass: 1, position: new Vector3(-1, 0, 0) });
        const c = new HingeConstraint(bodyA, bodyB, { maxForce: 123 });

        equal(c.equations.length, 6); // 5 actually, and 1 for the motor

        equal(c.equations[0].maxForce, 123);
        equal(c.equations[1].maxForce, 123);
        equal(c.equations[2].maxForce, 123);
        equal(c.equations[3].maxForce, 123);
        equal(c.equations[4].maxForce, 123);
        equal(c.equations[5].maxForce, 123);

        equal(c.equations[0].minForce, -123);
        equal(c.equations[1].minForce, -123);
        equal(c.equations[2].minForce, -123);
        equal(c.equations[3].minForce, -123);
        equal(c.equations[4].minForce, -123);
        equal(c.equations[5].minForce, -123);
    });

    it('update', () =>
    {
        const bodyA = new Body({ mass: 1, position: new Vector3(1, 0, 0) });
        const bodyB = new Body({ mass: 1, position: new Vector3(-1, 0, 0) });
        const c = new HingeConstraint(bodyA, bodyB, { maxForce: 123 });

        c.update();
        ok(true);
    });

    it('enableDisableMotor', () =>
    {
        const bodyA = new Body({ mass: 1, position: new Vector3(1, 0, 0) });
        const bodyB = new Body({ mass: 1, position: new Vector3(-1, 0, 0) });
        const c = new HingeConstraint(bodyA, bodyB);

        c.enableMotor();

        ok(c.motorEquation.enabled);

        c.disableMotor();

        equal(c.motorEquation.enabled, false);
    });

    it('setMotorSpeed', () =>
    {
        const bodyA = new Body({ mass: 1, position: new Vector3(1, 0, 0) });
        const bodyB = new Body({ mass: 1, position: new Vector3(-1, 0, 0) });
        const c = new HingeConstraint(bodyA, bodyB);

        c.setMotorSpeed(5);
        equal(c.motorEquation.targetVelocity, 5);
    });

    it('setMotorMaxForce', () =>
    {
        const bodyA = new Body({ mass: 1, position: new Vector3(1, 0, 0) });
        const bodyB = new Body({ mass: 1, position: new Vector3(-1, 0, 0) });
        const c = new HingeConstraint(bodyA, bodyB);

        c.setMotorMaxForce(100);
        equal(c.motorEquation.maxForce, 100);
    });
});
