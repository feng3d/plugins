import { Body } from '../../src/objects/Body';
import { Constraint } from '../../src/constraints/Constraint';
import { Equation } from '../../src/equations/Equation';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual } = assert;

describe('Constraint', () =>
{
    it('construct', () =>
    {
        const bodyA = new Body();
        const bodyB = new Body();
        new Constraint(bodyA, bodyB);
        ok(true);
    });

    it('enable', () =>
    {
        const bodyA = new Body();
        const bodyB = new Body();
        const c = new Constraint(bodyA, bodyB);
        const eq = new Equation(bodyA, bodyB);
        c.equations.push(eq);

        c.enable();
        ok(eq.enabled);

        c.disable();
        ok(!eq.enabled);
    });
});
