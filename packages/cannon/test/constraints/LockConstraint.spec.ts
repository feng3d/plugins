import { Vector3 } from 'feng3d';
import { LockConstraint } from '../../src/constraints/LockConstraint';
import { Body } from '../../src/objects/Body';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('LockConstraint', () =>
{
    it('construct', () =>
    {
        const bodyA = new Body({ mass: 1, position: new Vector3(1, 0, 0) });
        const bodyB = new Body({ mass: 1, position: new Vector3(-1, 0, 0) });
        const c = new LockConstraint(bodyA, bodyB, { maxForce: 123 });

        equal(c.equations.length, 6);

        equal(c.equations[0].maxForce, 123);
        equal(c.equations[1].maxForce, 123);
        equal(c.equations[2].maxForce, 123);
        equal(c.equations[3].maxForce, 123);
        equal(c.equations[4].maxForce, 123);
        equal(c.equations[5].maxForce, 123);

        equal(c.equations[0].minForce, -123);
        equal(c.equations[1].minForce, -123);
        equal(c.equations[2].minForce, -123);
        equal(c.equations[3].minForce, -123);
        equal(c.equations[4].minForce, -123);
        equal(c.equations[5].minForce, -123);
    });

    it('update', () =>
    {
        const bodyA = new Body({ mass: 1, position: new Vector3(1, 0, 0) });
        const bodyB = new Body({ mass: 1, position: new Vector3(-1, 0, 0) });
        const c = new LockConstraint(bodyA, bodyB, { maxForce: 123 });

        c.update();
        ok(true);
    });
});
