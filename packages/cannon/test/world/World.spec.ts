import { Vector3 } from 'feng3d';
import { RaycastResult } from '../../src/collision/RaycastResult';
import { Body } from '../../src/objects/Body';
import { Box } from '../../src/shapes/Box';
import { World } from '../../src/world/World';
import { Sphere } from '../../src/shapes/Sphere';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('World', () =>
{
    it('clearForces', () =>
    {
        const world = new World();
        const body = new Body();
        world.addBody(body);
        body.force.set(1, 2, 3);
        body.torque.set(4, 5, 6);

        world.clearForces();

        ok(body.force.equals(new Vector3(0, 0, 0)));
        ok(body.torque.equals(new Vector3(0, 0, 0)));
    });

    it('rayTestBox', () =>
    {
        const world = new World();

        const body = new Body();
        body.addShape(new Box(new Vector3(1, 1, 1)));
        world.addBody(body);

        const from = new Vector3(-10, 0, 0);
        const to = new Vector3(10, 0, 0);

        const result = new RaycastResult();
        world.raycastClosest(from, to, {}, result);

        equal(result.hasHit, true);
    });

    it('rayTestSphere', () =>
    {
        const world = new World();

        const body = new Body();
        body.addShape(new Sphere(1));
        world.addBody(body);

        const from = new Vector3(-10, 0, 0);
        const to = new Vector3(10, 0, 0);

        const result = new RaycastResult();
        world.raycastClosest(from, to, {}, result);

        equal(result.hasHit, true);
    });

    it('raycastClosest single', () =>
    {
        const world = new World();
        const body = new Body({
            shape: new Sphere(1)
        });
        world.addBody(body);

        const from = new Vector3(-10, 0, 0);
        const to = new Vector3(10, 0, 0);

        const result = new RaycastResult();
        world.raycastClosest(from, to, {}, result);

        equal(result.hasHit, true);
        equal(result.body, body);
        equal(result.shape, body.shapes[0]);
    });

    it('raycastClosest order', () =>
    {
        const world = new World();
        const bodyA = new Body({ shape: new Sphere(1), position: new Vector3(-1, 0, 0) });
        const bodyB = new Body({ shape: new Sphere(1), position: new Vector3(1, 0, 0) });
        world.addBody(bodyA);
        world.addBody(bodyB);

        const from = new Vector3(-10, 0, 0);
        const to = new Vector3(10, 0, 0);

        let result = new RaycastResult();
        world.raycastClosest(from, to, {}, result);

        equal(result.hasHit, true);
        equal(result.body, bodyA);
        equal(result.shape, bodyA.shapes[0]);

        from.set(10, 0, 0);
        to.set(-10, 0, 0);

        result = new RaycastResult();
        world.raycastClosest(from, to, {}, result);

        equal(result.hasHit, true);
        equal(result.body, bodyB);
        equal(result.shape, bodyB.shapes[0]);
    });

    it('raycastAll simple', () =>
    {
        const world = new World();
        const body = new Body({ shape: new Sphere(1) });
        world.addBody(body);

        const from = new Vector3(-10, 0, 0);
        const to = new Vector3(10, 0, 0);

        let hasHit;
        let numResults = 0;
        let resultBody;
        let resultShape;

        const returnVal = world.raycastAll(from, to, {}, function (result)
        {
            hasHit = result.hasHit;
            resultShape = result.shape;
            resultBody = result.body;
            numResults++;
        });

        equal(returnVal, true, 'should return true on hit');
        equal(hasHit, true);
        equal(resultBody, body);
        equal(numResults, 2);
        equal(resultShape, resultBody.shapes[0]);
    });

    it('raycastAll twoSpheres', () =>
    {
        const world = new World();
        const body = new Body({ shape: new Sphere(1) });
        world.addBody(body);

        const body2 = new Body({ shape: new Sphere(1) });
        world.addBody(body2);

        const from = new Vector3(-10, 0, 0);
        const to = new Vector3(10, 0, 0);

        let hasHit = false;
        let numResults = 0;
        let resultBody;
        let resultShape;

        world.raycastAll(from, to, {}, function (result)
        {
            hasHit = result.hasHit;
            resultShape = result.shape;
            resultBody = result.body;
            numResults++;
        });

        equal(hasHit, true);
        equal(numResults, 4);
    });

    it('raycastAll skipBackFaces', () =>
    {
        const world = new World();
        const body = new Body({ shape: new Sphere(1) });
        world.addBody(body);

        let hasHit = false;
        let numResults = 0;
        let resultBody;
        let resultShape;

        world.raycastAll(new Vector3(-10, 0, 0), new Vector3(10, 0, 0), { skipBackfaces: true }, function (result)
        {
            hasHit = result.hasHit;
            resultShape = result.shape;
            resultBody = result.body;
            numResults++;
        });

        equal(hasHit, true);
        equal(numResults, 1);
    });

    it('raycastAll collisionFilters', () =>
    {
        const world = new World();
        const body = new Body({
            shape: new Sphere(1)
        });
        world.addBody(body);
        body.collisionFilterGroup = 2;
        body.collisionFilterMask = 2;

        let numResults = 0;

        world.raycastAll(new Vector3(-10, 0, 0), new Vector3(10, 0, 0), {
            collisionFilterGroup: 2,
            collisionFilterMask: 2
        }, function (result)
        {
            numResults++;
        });

        equal(numResults, 2);

        numResults = 0;

        world.raycastAll(new Vector3(-10, 0, 0), new Vector3(10, 0, 0), {
            collisionFilterGroup: 1,
            collisionFilterMask: 1
        }, function (result)
        {
            numResults++;
        });

        equal(numResults, 0, 'should use collision groups!');
    });

    it('raycastAny', () =>
    {
        const world = new World();
        world.addBody(new Body({ shape: new Sphere(1) }));

        const from = new Vector3(-10, 0, 0);
        const to = new Vector3(10, 0, 0);

        const result = new RaycastResult();
        world.raycastAny(from, to, {}, result);

        ok(result.hasHit);
    });
});
