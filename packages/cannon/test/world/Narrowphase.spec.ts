import { Vector3, Quaternion } from 'feng3d';
import { ContactMaterial } from '../../src/material/ContactMaterial';
import { Body } from '../../src/objects/Body';
import { Heightfield } from '../../src/shapes/Heightfield';
import { Narrowphase } from '../../src/world/Narrowphase';
import { World } from '../../src/world/World';
import { Sphere } from '../../src/shapes/Sphere';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual, throws } = assert;

describe('Narrowphase', () =>
{
    it('sphereSphere', () =>
    {
        const world = new World();
        const cg = new Narrowphase(world);
        const result = [];
        const sphereShape = new Sphere(1);

        const bodyA = new Body({ mass: 1 });
        bodyA.addShape(sphereShape);
        const bodyB = new Body({ mass: 1 });
        bodyB.addShape(sphereShape);

        cg.currentContactMaterial = new ContactMaterial();
        cg.result = result;
        cg.sphereSphere(
            sphereShape,
            sphereShape,
            new Vector3(0.5, 0, 0),
            new Vector3(-0.5, 0, 0),
            new Quaternion(),
            new Quaternion(),
            bodyA,
            bodyB
        );

        equal(result.length, 1);
    });

    it('sphereHeightfield', () =>
    {
        const world = new World();
        const cg = new Narrowphase(world);
        const result = [];
        const hfShape = createHeightfield();
        const sphereShape = new Sphere(0.1);
        cg.currentContactMaterial = new ContactMaterial();
        cg.result = result;
        cg.sphereHeightfield(
            sphereShape,
            hfShape,
            new Vector3(0.25, 0.25, 0.05), // hit the first triangle in the field
            new Vector3(0, 0, 0),
            new Quaternion(),
            new Quaternion(),
            new Body(),
            new Body()
        );

        equal(result.length, 1);
    });
});

function createHeightfield()
{
    const matrix: number[][] = [];
    const size = 20;
    for (let i = 0; i < size; i++)
    {
        matrix.push([]);
        for (let j = 0; j < size; j++)
        {
            matrix[i].push(0);
        }
    }
    const hfShape = new Heightfield(matrix, {
        elementSize: 1,
    });

    return hfShape;
}
