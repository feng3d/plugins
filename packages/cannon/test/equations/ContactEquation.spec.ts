import { Vector3 } from 'feng3d';
import { ContactEquation } from '../../src/equations/ContactEquation';
import { Body } from '../../src/objects/Body';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual } = assert;

describe('ContactEquation', () =>
{
    it('construct', () =>
    {
        const bodyA = new Body();
        const bodyB = new Body();
        new ContactEquation(bodyA, bodyB);
        ok(true);
    });

    it('getImpactVelocityAlongNormal', () =>
    {
        const bodyA = new Body({
            position: new Vector3(1, 0, 0),
            velocity: new Vector3(-10, 0, 0)
        });
        const bodyB = new Body({
            position: new Vector3(-1, 0, 0),
            velocity: new Vector3(1, 0, 0)
        });
        const contact = new ContactEquation(bodyA, bodyB);
        contact.ni.set(1, 0, 0);
        contact.ri.set(-1, 0, 0);
        contact.rj.set(1, 0, 0);
        const v = contact.getImpactVelocityAlongNormal();
        equal(v, -11);
    });
});
