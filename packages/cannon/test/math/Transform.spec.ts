import { Box3, Vector3 } from 'feng3d';
import { Transform } from '../../src/math/Transform';

import { assert, describe, it } from 'vitest';
const { ok, equal, deepEqual } = assert;

describe('Transform', () =>
{
    it('toLocalFrame', () =>
    {
        const worldAABB = new Box3();
        const localAABB = new Box3();
        const frame = new Transform();

        worldAABB.min.set(-1, -1, -1);
        worldAABB.max.set(1, 1, 1);

        // No transform - should stay the same
        frame.toLocalFrameBox3(worldAABB, localAABB);
        deepEqual(localAABB, worldAABB);

        // Some translation
        frame.position.set(-1, 0, 0);
        frame.toLocalFrameBox3(worldAABB, localAABB);
        deepEqual(
            localAABB,
            new Box3(new Vector3(0, -1, -1), new Vector3(2, 1, 1))
        );
    });

    it('toWorldFrame', () =>
    {
        const localAABB = new Box3();
        const worldAABB = new Box3();
        const frame = new Transform();

        localAABB.min.set(-1, -1, -1);
        localAABB.max.set(1, 1, 1);

        // No transform - should stay the same
        frame.toLocalFrameBox3(localAABB, worldAABB);
        deepEqual(localAABB, worldAABB);

        // Some translation on the frame
        frame.position.set(1, 0, 0);
        frame.toWorldFrameBox3(localAABB, worldAABB);
        deepEqual(
            worldAABB,
            new Box3(new Vector3(0, -1, -1), new Vector3(2, 1, 1))
        );
    });
});
