// @see https://cn.vitejs.dev/guide/build.html#library-mode

import { resolve } from 'path';
import { defineConfig } from 'vite';
import pkg from './package.json';

export default defineConfig({
    publicDir: false,
    build: {
        lib: {
            // Could also be a dictionary or array of multiple entry points
            entry: resolve(__dirname, 'src/index.ts'),
            name: pkg.namespace,
            // the proper extensions will be added
            fileName: 'index'
        },
        sourcemap: true,
        rollupOptions: {
            external: ['feng3d'],
            output: {
                globals: { feng3d: 'feng3d' }
            }
        }
    },
});
