import { globalEmitter, pathUtils } from 'feng3d';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader.js';
import { parseObject3D } from './parsers/parseObject3D';

export async function load(url: string)
{
    const loader = new FBXLoader();
    const object = await loader.loadAsync(url);

    const node3d = parseObject3D(object);

    node3d.name = pathUtils.nameWithOutExt(url);
    globalEmitter.emit('asset.parsed', node3d);

    return node3d;
}
