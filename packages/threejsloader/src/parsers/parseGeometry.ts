import { CustomGeometry, geometryUtils } from 'feng3d';
import { BufferGeometry } from 'three';

export function parseGeometry(geometry: BufferGeometry)
{
    const attributes = geometry.attributes;

    const geo = new CustomGeometry();

    for (const key in attributes)
    {
        if (attributes.hasOwnProperty(key))
        {
            const element = attributes[key];
            let array: number[] = Array.from(element.array);
            array = array.map((v: number) => Number(v.toFixed(6)));
            switch (key)
            {
                case 'position':
                    geo.attributes.a_position = { array, itemSize: 3 };
                    break;
                case 'normal':
                    geo.attributes.a_normal = { array, itemSize: 3 };
                    break;
                case 'uv':
                    geo.attributes.a_uv = { array, itemSize: 2 };
                    break;
                case 'skinIndex':
                    geo.attributes.a_skinIndices = { array, itemSize: 4 };
                    break;
                case 'skinWeight':
                    geo.attributes.a_skinWeights = { array, itemSize: 4 };
                    break;
                default:
                    console.warn('没有解析顶点数据', key);
                    break;
            }
        }
    }

    if (geometry.index)
    {
        geo.indexBuffer = { array: geometry.index.array as any };
    }

    if (!geo.indexBuffer)
    {
        const indices = geometryUtils.createIndices(geo.attributes.a_position.array as any);
        geo.indexBuffer = { array: indices };
    }
    if (!geo.attributes.a_normal.array.length)
    {
        const indices = geo.indexBuffer.array as any;
        const positions = geo.attributes.a_position.array as any;

        const normals = geometryUtils.createVertexNormals(indices, positions);

        geo.attributes.a_normal = {
            array: normals, itemSize: 3
        };
    }
    if (!geo.attributes.a_uv.array.length)
    {
        const positions = geo.attributes.a_position.array as any;
        const uvs = geometryUtils.createUVs(positions);

        geo.attributes.a_uv = { array: uvs, itemSize: 2 };
    }
    if (!geo.attributes.a_tangent.array.length)
    {
        const indices = geo.indexBuffer.array as any;
        const positions = geo.attributes.a_position.array as any;
        const uvs = geo.attributes.a_uv.array as any;

        const tangents = geometryUtils.createVertexTangents(indices, positions, uvs);
        geo.attributes.a_tangent = { array: tangents, itemSize: 3 };
    }

    return geo;
}
