import { MeshRenderer, Node3D, StandardMaterial } from 'feng3d';
import { Mesh } from 'three';
import { parseGeometry } from './parseGeometry';

export function parseMesh(mesh: Mesh, node3d: Node3D)
{
    const model = node3d.addComponent(MeshRenderer);
    model.geometry = parseGeometry(mesh.geometry);
    model.material = new StandardMaterial();
    model.material.renderParams.cullFace = 'NONE';
}
