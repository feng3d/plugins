import { Node3D, Quaternion, Vector3 } from 'feng3d';
import * as THREE from 'three';

export function parseTransform(object3d: THREE.Object3D, node3d: Node3D)
{
    node3d.position = new Vector3(object3d.position.x, object3d.position.y, object3d.position.z);
    node3d.orientation = new Quaternion(object3d.quaternion.x, object3d.quaternion.y, object3d.quaternion.z, object3d.quaternion.w);
    node3d.scale = new Vector3(object3d.scale.x, object3d.scale.y, object3d.scale.z);
}
