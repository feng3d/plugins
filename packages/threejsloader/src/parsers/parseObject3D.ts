import { $set, Node3D } from 'feng3d';
import * as THREE from 'three';
import { Mesh, PerspectiveCamera, SkinnedMesh } from 'three';
import { parseAnimations } from './parseAnimations';
import { parseMesh } from './parseMesh';
import { parsePerspectiveCamera } from './parsePerspectiveCamera';
import { parseSkinnedMesh } from './parseSkinnedMesh';
import { parseTransform } from './parseTransform';

export function parseObject3D(object3d: THREE.Object3D, parent?: Node3D): Node3D
{
    const node3d = $set(new Node3D(), { name: object3d.name });
    parseTransform(object3d, node3d);

    if (parent)
    {
        parent.addChild(node3d);
    }

    switch (object3d.type)
    {
        case 'PerspectiveCamera':
            parsePerspectiveCamera(object3d as PerspectiveCamera, node3d);
            break;
        case 'SkinnedMesh':
            parseSkinnedMesh(object3d as SkinnedMesh, node3d);
            break;
        case 'Mesh':
            parseMesh(object3d as any as Mesh, node3d);
            break;
        case 'Group':
            break;
        case 'Bone':
            // Bone 由SkeletonComponent自动生成，不用解析
            break;
        default:
            console.warn(`没有提供 ${object3d.type} 类型对象的解析`);
            break;
    }

    parseAnimations(object3d, node3d);

    object3d.children.forEach((element) =>
    {
        parseObject3D(element, node3d);
    });

    return node3d;
}
