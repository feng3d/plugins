import { MeshRenderer, Node3D, SkeletonComponent, SkinnedMeshRenderer, StandardMaterial } from 'feng3d';
import { SkinnedMesh } from 'three';
import { parseGeometry } from './parseGeometry';
import { parseSkinnedSkeleton } from './parseSkinnedSkeleton';

export function parseSkinnedMesh(skinnedMesh: SkinnedMesh, node3d: Node3D)
{
    const skinnedModel = node3d.addComponent(SkinnedMeshRenderer).getComponent(MeshRenderer);
    skinnedModel.geometry = parseGeometry(skinnedMesh.geometry);
    skinnedModel.material = new StandardMaterial();
    skinnedModel.material.renderParams.cullFace = 'NONE';
    console.assert(skinnedMesh.bindMode === 'attached');
    const skeletonComponent = node3d.getComponentInParent(SkeletonComponent);
    parseSkinnedSkeleton(skeletonComponent, skinnedMesh.skeleton);
}
