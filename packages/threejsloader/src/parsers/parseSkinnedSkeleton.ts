import { Matrix4x4, SkeletonComponent } from 'feng3d';
import { Skeleton } from 'three';

export function parseSkinnedSkeleton(skeleton: SkeletonComponent, skinSkeletonData: Skeleton)
{
    skeleton.boneInverses = skinSkeletonData.boneInverses.map((v) => new Matrix4x4(v.elements as any));

    skeleton.boneNames = skinSkeletonData.bones.map((v) => v.name);
}
