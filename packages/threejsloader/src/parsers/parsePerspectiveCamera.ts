import { Camera, Node3D, PerspectiveLens } from 'feng3d';
import { PerspectiveCamera } from 'three';

export function parsePerspectiveCamera(perspectiveCamera: PerspectiveCamera, node3d: Node3D)
{
    const perspectiveLen = new PerspectiveLens();

    perspectiveLen.near = perspectiveCamera.near;
    perspectiveLen.far = perspectiveCamera.far;
    perspectiveLen.aspect = perspectiveCamera.aspect;
    perspectiveLen.fov = perspectiveCamera.fov;

    node3d.addComponent(Camera).lens = perspectiveLen;
}
