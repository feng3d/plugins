import { Animation, AudioListener, Camera, DirectionalLight, FPSController, Node3D, Scene, ShadowType, Vector3, View3D } from 'feng3d';
import { load } from './src';

const scene = new Node3D().addComponent(Scene);
scene.background.setTo(0.2784, 0.2784, 0.2784);
scene.ambientColor.setTo(0.4, 0.4, 0.4);

const camera = new Node3D().addComponent(Camera).node3d;
camera.addComponent(AudioListener);
camera.addComponent(FPSController);
camera.position = new Vector3(100, 200, 300);
camera.lookAt(new Vector3(), Vector3.Y_AXIS);
scene.node3d.addChild(camera);

const directionalLight = new Node3D();
directionalLight.addComponent(DirectionalLight).shadowType = ShadowType.Hard_Shadows;
directionalLight.rx = 50;
directionalLight.ry = -30;
directionalLight.y = 3;
scene.node3d.addChild(directionalLight);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const engine = new View3D(undefined, scene);

// const cube = Node3D.createPrimitive('Cube');
// scene.node3d.addChild(cube);

load('models/fbx/Samba Dancing.fbx').then((node3d) =>
{
    scene.node3d.addChild(node3d);

    const animation = node3d.getComponent(Animation);
    animation.isplaying = true;

    console.log(node3d);
});
